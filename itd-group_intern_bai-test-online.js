function MyBigNumber(stn1, stn2) {
    let result = "";
    let carry = 0;
    for (let i = 1; i <= Math.max(stn1.length, stn2.length); i++) {
      let digit1 = parseInt(stn1[stn1.length - i]) || 0;
      console.log(digit1);
      let digit2 = parseInt(stn2[stn2.length - i]) || 0;
      console.log(digit2);
      let Sum = digit1 + digit2 + carry;
      console.log(Sum);
      result += Sum % 10;
      carry = Math.floor(Sum / 10);
    }
    if (carry !== 0) {
      result += carry;
    }
    return result.split("").reverse().join("");
  }
  let stn1 = "1234";
  let stn2 = "897";
  let result = MyBigNumber(stn1, stn2);
  console.log(result);
  